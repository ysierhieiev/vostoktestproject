#include "Objects/VTTYellowBall.h"

AVTTYellowBall::AVTTYellowBall()
{
	BallType = EVTTBallType::YellowBall;
}

void AVTTYellowBall::ProcessBorderCollision(FVector const& ImpactNormal)
{
	SetBallForwardDirection(ImpactNormal.RotateAngleAxis(FMath::RandRange(-90.f, 90.f), GetActorUpVector()));
	
	SetBallVelocity((GetBallForwardDirection() * BallSpeed * BallMass));
	
	if (!(--MaxCollisionsWithBorders))
	{
		Destroy();
	}
}
