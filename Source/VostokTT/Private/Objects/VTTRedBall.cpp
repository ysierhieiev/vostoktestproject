#include "Objects/VTTRedBall.h"

AVTTRedBall::AVTTRedBall()
{
	BallType = EVTTBallType::RedBall;
}

void AVTTRedBall::ProcessBallCollision(FVector const& CollisionImpulseAxis, FVector const& OtherBallVelocity,	float OtherBallMass)
{
	AVTTBallBase::ProcessBallCollision(CollisionImpulseAxis, OtherBallVelocity, OtherBallMass);
	
	SetBallMass(this->GetBallMass() + OtherBallMass);
	
	if (GetBallMass() > MaxBallMass)
	{
		Destroy();
	}
}
