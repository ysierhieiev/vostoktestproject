#include "Objects/VTTTableBase.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "VostokTT/Public/Objects/VTTBallBase.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"

AVTTTableBase::AVTTTableBase()
{
 	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SpawnAreaBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionSphere"));
	SpawnAreaBox->SetupAttachment(RootComponent);
	SpawnAreaBox->SetRelativeLocation(FVector(0.f, 0.f, 155.f));
	SpawnAreaBox->SetRelativeScale3D(FVector(1.f));
}

void AVTTTableBase::BeginPlay()
{
	Super::BeginPlay();	

	SpawnLocationZ = SpawnAreaBox->GetComponentLocation().Z;
	
	if (AvailableToSpawnBalls.Num() > 0)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_SpawnBall, this, &AVTTTableBase::SpawnBall, SpawnFrequency);
	}
}

void AVTTTableBase::SpawnBall()
{
	const int RandomIndex = FMath::RandRange(0, AvailableToSpawnBalls.Num() - 1);
	
	FVector SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(SpawnAreaBox->GetComponentLocation(), SpawnAreaBox->GetScaledBoxExtent());
	SpawnLocation.Z = SpawnLocationZ;
	FActorSpawnParameters BallSpawnParameter;
	BallSpawnParameter.Owner = this;
	BallSpawnParameter.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	auto CurrentBall = GetWorld()->SpawnActor<AVTTBallBase>(AvailableToSpawnBalls[RandomIndex], SpawnLocation, FRotator(0.f), BallSpawnParameter);
	
	if(CurrentBall)
	{
		CurrentBall->OnDestroyed.AddDynamic(this, &AVTTTableBase::BallDestroyed);
		Balls.Add(CurrentBall);
	}
	
	if (Balls.Num() < MaxSpawnedBalls)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_SpawnBall, this, &AVTTTableBase::SpawnBall, SpawnFrequency);
	}
}

void AVTTTableBase::BallDestroyed(AActor* DestroyedActor)
{
	AVTTBallBase* DestroyedBall = Cast<AVTTBallBase>(DestroyedActor);
	if (DestroyedBall)
	{
		Balls.Remove(DestroyedBall);
		
		if(!GetWorldTimerManager().IsTimerActive(TimerHandle_SpawnBall))
		{
			GetWorldTimerManager().SetTimer(TimerHandle_SpawnBall, this, &AVTTTableBase::SpawnBall, SpawnFrequency);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed cast to VTTBallBase!!!"));
	}
}
