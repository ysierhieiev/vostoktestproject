#include "Objects/VTTBallBase.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"

AVTTBallBase::AVTTBallBase()
{
 	PrimaryActorTick.bCanEverTick = true;

	RootComponent	= CreateDefaultSubobject<USceneComponent>( TEXT("RootComponent") );

	CollisionSphere = CreateDefaultSubobject<USphereComponent>( TEXT("CollisionSphere") );
	
	CollisionSphere	->SetupAttachment( RootComponent );
	CollisionSphere	->SetRelativeScale3D( FVector( 1.f ) );
	CollisionSphere ->SetCollisionProfileName(UCollisionProfile::BlockAllDynamic_ProfileName);

	VisualSphere = CreateDefaultSubobject<UStaticMeshComponent>( TEXT("VisualSphere") );
	
	VisualSphere ->SetupAttachment( CollisionSphere );
	VisualSphere ->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
}

// Called when the game starts or when spawned
void AVTTBallBase::BeginPlay()
{
	Super::BeginPlay();

	BallMass = FMath::RandRange(0.1f, 20.f);
	BallSpeed = FMath::RandRange(MinBallSpeed, MaxBallSpeed);
	
	CurrentForwardDirection = FMath::VRand();
	CurrentForwardDirection.Z = 0.f;
	CurrentVelocity = CurrentForwardDirection * BallSpeed * BallMass;

}
void AVTTBallBase::ProcessBorderCollision(FVector const& ImpactNormal)
{
	CurrentForwardDirection = FMath::GetReflectionVector(CurrentForwardDirection, ImpactNormal);
	CurrentVelocity = CurrentForwardDirection * BallSpeed * BallMass;

	if(!(--MaxCollisionsWithBorders))
	{
		Destroy();
	}	
}

void AVTTBallBase::ProcessBallCollision(FVector const& CollisionImpulseAxis, FVector const& SecondBallVelocity, float SecondBallMass )
{	
	CurrentForwardDirection = FMath::GetReflectionVector(CurrentForwardDirection, CollisionImpulseAxis);

	CurrentVelocity = CurrentForwardDirection * BallSpeed * BallMass;

	AlreadyCollidedWithBall = true;
	
	if (!(--MaxCollisionsWithBalls))
	{
		Destroy();
	}
}

void AVTTBallBase::	ProcessBallMovement(float const DeltaTime)
{
	SetActorLocation( GetActorLocation() + CurrentVelocity * DeltaTime );
}

void AVTTBallBase::DoCollisionTest(float const DeltaTime)
{
	
	
	FVector const& SweepFrom	= GetActorLocation();
	FVector const& SweepTo		= GetActorLocation() + CurrentVelocity * DeltaTime;

	const TArray<AActor*>		ActorsToIgnore({ this });
	
	FHitResult					SweepResult;
	
	UKismetSystemLibrary::SphereTraceSingle( this, SweepFrom, SweepTo, 
											CollisionSphere->GetScaledSphereRadius(), 
											ETraceTypeQuery::TraceTypeQuery1, 
											false, 
											ActorsToIgnore, 											
											EDrawDebugTrace::None, 
											SweepResult,
											true
											);

	if ( SweepResult.bBlockingHit )
	{
		if ( AVTTBallBase* Ball = Cast<AVTTBallBase>( SweepResult.GetActor() ) )
		{
			if ( !AlreadyCollidedWithBall )
			{
				FVector FirstBallCachedVelocity		= CurrentVelocity;
				FVector SecondBallCachedVelocity	= Ball->GetBallVelocity();
				float	FirstBallCachedMass			= BallMass;
				float	SecondBallCachedMass		= Ball->GetBallMass();

				FVector CollisionImpulseAxis = (GetActorLocation() - Ball->GetActorLocation()).GetSafeNormal();
				CollisionImpulseAxis.Z = 0.f;
				
				this->ProcessBallCollision(CollisionImpulseAxis, SecondBallCachedVelocity, SecondBallCachedMass );
				this->NotifyOnBallHit(Ball);
				Ball->ProcessBallCollision(CollisionImpulseAxis, FirstBallCachedVelocity, FirstBallCachedMass );
				Ball->NotifyOnBallHit(this);
			}
		}
		else
			ProcessBorderCollision( SweepResult.ImpactNormal );
	}

	AlreadyCollidedWithBall = false;	
}

void AVTTBallBase::NotifyOnBallHit(AVTTBallBase* OtherBall)
{
	if (OnBallHit.IsBound())
	{
		OnBallHit.Broadcast(OtherBall);
	}
}

// Called every frame
void AVTTBallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DoCollisionTest		( DeltaTime );
	ProcessBallMovement	( DeltaTime );	
}