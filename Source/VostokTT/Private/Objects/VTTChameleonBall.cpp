#include "Objects/VTTChameleonBall.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

AVTTChameleonBall::AVTTChameleonBall()
{
	BallType = EVTTBallType::ChameleonBall;
}

void AVTTChameleonBall::BeginPlay()
{
	Super::BeginPlay();

	OnBallHit.AddDynamic(this, &AVTTChameleonBall::OnChameleonBallHit);
}

void AVTTChameleonBall::OnChameleonBallHit(AActor* OtherActor)
{
	AVTTBallBase* OtherBall = Cast<AVTTBallBase>(OtherActor);
	if (OtherBall)
	{
		const auto CurrentMaterial = GetVisualSphere()->GetMaterial(0)->GetMaterial();
		if (CurrentMaterial)
		{
			UMaterialInstanceDynamic* NewBallColor = UMaterialInstanceDynamic::Create(CurrentMaterial, OtherBall);
			OtherBall->GetVisualSphere()->SetMaterial(0, NewBallColor);
		}
	}
}
