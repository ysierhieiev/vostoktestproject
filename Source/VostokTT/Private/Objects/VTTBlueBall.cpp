#include "Objects/VTTBlueBall.h"

AVTTBlueBall::AVTTBlueBall()
{
	BallType = EVTTBallType::BlueBall;
}

void AVTTBlueBall::BeginPlay()
{
	Super::BeginPlay();

	CurrentRightDirection = GetBallForwardDirection().RotateAngleAxis(90.f, GetActorUpVector());
}

void AVTTBlueBall::ProcessBallMovement(float const DeltaTime)
{	
	CurrentRightVelocity = CurrentRightDirection * (BallMass * BallSpeed * FMath::Sin(GetGameTimeSinceCreation() * 4.f));
	
	SetActorLocation(GetActorLocation() + (CurrentRightVelocity + GetBallVelocity()) * DeltaTime);		
}

void AVTTBlueBall::ProcessBallCollision(FVector const& CollisionImpulseAxis, FVector const& OtherBallVelocity, float OtherBallMass)
{
	AVTTBallBase::ProcessBallCollision(CollisionImpulseAxis, OtherBallVelocity, OtherBallMass);
	
	CurrentRightDirection = GetBallForwardDirection().RotateAngleAxis(90.f, GetActorUpVector());

	CurrentRightVelocity = CurrentRightDirection * (BallMass * BallSpeed * FMath::Sin(GetGameTimeSinceCreation() * 4.f));


	if (FVector::DotProduct(CollisionImpulseAxis, CurrentRightVelocity) < 0.f)
	{
		CurrentRightDirection *= -1;
	}
}

void AVTTBlueBall::ProcessBorderCollision(FVector const& ImpactNormal)
{
	AVTTBallBase::ProcessBorderCollision(ImpactNormal);

	CurrentRightDirection = GetBallForwardDirection().RotateAngleAxis(90.f, GetActorUpVector());

	CurrentRightVelocity = CurrentRightDirection * (BallMass * BallSpeed * FMath::Sin(GetGameTimeSinceCreation() * 4.f));

	if (FVector::DotProduct(ImpactNormal, CurrentRightVelocity) < 0.f)
	{
		CurrentRightDirection *= -1;
	}
}
