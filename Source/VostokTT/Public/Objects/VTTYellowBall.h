#pragma once

#include "CoreMinimal.h"
#include "Objects/VTTBallBase.h"
#include "VTTYellowBall.generated.h"

UCLASS()
class VOSTOKTT_API AVTTYellowBall : public AVTTBallBase
{
	GENERATED_BODY()

public:
	
	AVTTYellowBall();

protected:
	
	void ProcessBorderCollision(FVector const& ImpactNormal) override;
	
};
