// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/VTTBallBase.h"
#include "VTTRedBall.generated.h"

/**
 * 
 */
UCLASS()
class VOSTOKTT_API AVTTRedBall : public AVTTBallBase
{
	GENERATED_BODY()

public:

	AVTTRedBall();
	
protected:

	UPROPERTY(EditDefaultsOnly, Category = Default)
	float MaxBallMass			= 100.f;
	
	void						ProcessBallCollision(FVector const& CollisionImpulseAxis, FVector const& OtherBallVelocity, float OtherBallMass) override;
	
};
