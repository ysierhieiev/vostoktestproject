#pragma once

#include "CoreMinimal.h"
#include "Objects/VTTBallBase.h"
#include "VTTChameleonBall.generated.h"

UCLASS()
class VOSTOKTT_API AVTTChameleonBall : public AVTTBallBase
{
	GENERATED_BODY()

public:

	AVTTChameleonBall();
	
protected:
	
	virtual void				BeginPlay() override;

	UFUNCTION()
	void						OnChameleonBallHit(AActor* OtherActor);
};
