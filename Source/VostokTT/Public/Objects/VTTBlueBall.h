#pragma once

#include "CoreMinimal.h"
#include "Objects/VTTBallBase.h"
#include "VTTBlueBall.generated.h"

UCLASS()
class VOSTOKTT_API AVTTBlueBall : public AVTTBallBase
{
	GENERATED_BODY()


public:

	AVTTBlueBall();
	
protected:

	void BeginPlay() override;
	
	void				ProcessBallMovement(float const DeltaTime) override;

	void				ProcessBallCollision(FVector const& CollisionImpulseAxis, FVector const& OtherBallVelocity, float OtherBallMass) override;
	
	void				ProcessBorderCollision(FVector const& ImpactNormal) override;

public:
	UFUNCTION(BlueprintPure)
	FORCEINLINE FVector const&  GetBallRightDirection() const							{ return CurrentRightDirection; }
	FORCEINLINE void			SetBallRightDirection(FVector const& NewRightDirection) { CurrentRightDirection = NewRightDirection; }


	UFUNCTION(BlueprintPure)
	FORCEINLINE FVector const&  GetBallRightVelocity() const							{ return CurrentRightVelocity; }
	FORCEINLINE void			SetBallRightVelocity(FVector const& NewRightVelocity)	{ CurrentRightVelocity = NewRightVelocity; }
	
private:
	
	FVector CurrentRightDirection;	
	FVector CurrentRightVelocity;
};
