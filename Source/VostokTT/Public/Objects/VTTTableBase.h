#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"	
#include "VTTTableBase.generated.h"


class AVTTBallBase;
class UBoxComponent;

UCLASS()
class VOSTOKTT_API AVTTTableBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AVTTTableBase();

protected:
	
	virtual void BeginPlay() override;

	UFUNCTION()
	void SpawnBall();

	UFUNCTION()
	void BallDestroyed(AActor* DestroyedActor);

	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Default)
	UBoxComponent* SpawnAreaBox = nullptr;
	
	UPROPERTY(EditDefaultsOnly, Category = Default)
	float SpawnLocationZ = 0.f;
	
protected:
	
	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "0.0"), Category = Default)
	float SpawnFrequency = 1.f;	

	UPROPERTY(EditDefaultsOnly, Category = Default)
	TArray<TSubclassOf<AVTTBallBase>> AvailableToSpawnBalls;
	
	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "0"), Category = Default)
	int32 MaxSpawnedBalls = 5;

	FTimerHandle TimerHandle_SpawnBall;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<AVTTBallBase*> Balls;

};
