#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VTTBallBase.generated.h"

//For UI
UENUM(BlueprintType)
enum class EVTTBallType : uint8
{
	BallBase,
	YellowBall,
	ChameleonBall,
	BlueBall,
	RedBall
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBallHitSignature, AActor*, OtherActor);

class USphereComponent;

UCLASS()
class VOSTOKTT_API AVTTBallBase : public AActor
{
	GENERATED_BODY()
private:
	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Meta=(AllowPrivateAccess="true"), Category = Components)
	USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* VisualSphere = nullptr;
	
public:	

	AVTTBallBase();
	
protected:

	virtual void				BeginPlay() override;

	virtual void				ProcessBorderCollision	( FVector const& ImpactNormal );
	virtual void				ProcessBallCollision	(FVector const& CollisionImpulseAxis, FVector const& OtherBallVelocity, float OtherBallMass );
	virtual void				ProcessBallMovement		( float const DeltaTime );	
	virtual void				DoCollisionTest			( float const DeltaTime );

	void						NotifyOnBallHit			(AVTTBallBase* OtherBall);
	
public:	

	virtual void				Tick(float DeltaTime) override;

	FORCEINLINE UStaticMeshComponent* GetVisualSphere() const				{ return VisualSphere; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE float			GetBallMass() const							{ return BallMass; }
	FORCEINLINE void			SetBallMass(float const& NewBallMass)		{ BallMass = NewBallMass; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE float			GetBallSpeed() const						{ return BallSpeed; }
	FORCEINLINE float			GetMaxCollisionsWithBorders() const			{ return MaxCollisionsWithBorders; }
	FORCEINLINE float			GetMaxCollisionsWithBalls() const			{ return MaxCollisionsWithBalls; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE FVector const&  GetBallForwardDirection() const				{ return CurrentForwardDirection; }
	FORCEINLINE void			SetBallForwardDirection(FVector const& NewForwardDirection) { CurrentForwardDirection = NewForwardDirection; }

	
	UFUNCTION(BlueprintPure)
	FORCEINLINE FVector const&	GetBallVelocity() const						{ return CurrentVelocity; }
	FORCEINLINE void			SetBallVelocity(FVector const& NewVelocity)	{ CurrentVelocity = NewVelocity; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE EVTTBallType	GetBallType() const { return BallType; }
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Default)
	EVTTBallType BallType				= EVTTBallType::BallBase;
	
	UPROPERTY(BlueprintAssignable)
	FOnBallHitSignature OnBallHit;
	
	UPROPERTY(VisibleAnywhere)
	float BallMass= 1.f;
	
	UPROPERTY(VisibleAnywhere)
	float BallSpeed = 100.f;
	
	UPROPERTY(EditDefaultsOnly, Category = Default)
	int MaxCollisionsWithBorders		= 20;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	int MaxCollisionsWithBalls			= 20;

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "0.0"), Category = Default)
	float MinBallSpeed					= 10.f;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	float MaxBallSpeed					= 100.f;
	
private:
	FVector CurrentForwardDirection		= FVector::ZeroVector;
	FVector CurrentVelocity				= FVector::ZeroVector;
	bool AlreadyCollidedWithBall		= false;
};
